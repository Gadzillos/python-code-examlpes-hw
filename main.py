# ДЗ Python примеры кода. Стоило закомментить и объяснить код, но лучше потратить это время на дз 2 :)
# >>Пример 1
# Использование Jupiter Notebooks и Python для анализа данных из CSV файла
# Благодаря отличному математическому ядру Python и куче библиотек остаётся только понимать математику и ML, не прибегая к "сложному" коду
# Получаем с pandas Dataframe и делаем с ним, что хотим. В данном случае исследуем параметры нейтронных звезд 

!wget https://courses.openedu.ru/assets/courseware/v1/7a1926ffa573f869ba01e7bb47893a1b/asset-v1:ITMOUniversity+MLDATAN+spring_2020_ITMO_bac+type@asset+block/pulsar_stars_new.csv

import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier

df = pd.read_csv('pulsar_stars_new.csv') 
df.head(3)

a = df[df.TARGET == 0]
a = a[a.MIP >= 87.5859375]
a = a[a.MIP <= 88.484375]

b = df[df.TARGET == 1]
b = b[b.MIP >= 14.1484375]
b = b[b.MIP <= 20.7890625]

df = pd.concat([a, b])

print(f'Число строк {df.shape[0]}')

print(f'Выборочное среднее: {df.MIP.mean():.3f}')

X = df.iloc[:, df.columns != 'TARGET']
y = df.TARGET

#Прекрасная линейная нормировка
X = (X - X.min()) / (X.max() - X.min())

print(f'Выборочное среднее MIP после нормировки: {X.MIP.mean():.3f}')

clf = LogisticRegression(random_state=2019)
clf.fit(X, y)

obj = [0.142, 0.324, 0.6, 0.579, 0.124, 0.302, 0.309, 0.231]

print(f'LogisticRegression probability Pulsar: {clf.predict_proba([obj])[0][1]:.3f}')

clf = KNeighborsClassifier()
clf.fit(X, y)

print(f'KNeighborsClassifier расстояние: {clf.kneighbors([obj])[0][0][0]:.3f}')


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Пример 2 - факториал с помощью библиотеки

import math
  
print ("The factorial of 23 is : ", end="")
print (math.factorial(23))


# 2.1 Классическая задача нахождение факториала
n = 23
fact = 1
  
for i in range(1,n+1):
    fact = fact * i
      
print ("The factorial of 23 is : ", fact)
print ("The factorial of 23 is : " + str(fact)) # Конкатенация + приведение типов


# 2.2 Рекурсивное нахождение факториала
def factorial(n):
      if n == 1:   # Termination condition
            return 1    # Base case
      else:
            res = n * factorial (n-1)   # Recursive Call
            return res

print (factorial(5))


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Python лучший движок для игр. Готовьте ваши терминалы и FOSS
# Простая игра без ООП на функциях. Есть ещё ООП версии этой игры с Abstract Factory, но много кода.

import random
import sys

monster_counter = 0
hp = 10
attack = 10

monster_hp = 0
monster_attack = 0


def choice_player() -> str:
    """Выбор игрока."""
    pl_choice = input()
    while pl_choice != "1" and pl_choice != "2":
        pl_choice = input()
        if pl_choice != "1" and pl_choice != "2":
            print("Ты должен ввести 1 или 2")

    return pl_choice
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def hero() -> None:
    """Показывает текущие параметры героя."""
    print("Показатель очков здоровья героя:", str(hp), "HP")
    print("Показатель атаки героя:", str(attack), "damage")


def monster() -> None:
    """Генерирует нового монстра и показывает его параметры."""
    global monster_hp
    global monster_attack
    monster_hp = random.randint(1, 5)
    monster_attack = random.randint(1, 5)
    print("Показатель очков здоровья монстра:", str(monster_hp), "HP")
    print("Показатель атаки монстра:", str(monster_attack), "damage")


def apple() -> None:
    """Создает яблоко и увеличивает хп."""
    increase_hp = random.randint(1, 5)
    global hp
    print("вы нашли яблоко, очки здоровья увеличены на", str(increase_hp), "HP")
    hp = hp + increase_hp
    print("Текущий показатель здоровья равен ", str(hp), "HP")


def sword() -> None:
    """Подбор меча и выбор игрока, брать его или нет."""
    increase_attack = random.randint(1, 25)
    global attack

    print(
        "Вы нашли клинок, с уроном",
        str(increase_attack),
        "\nЧто вы будете делать с этим клинком? "
        "\n1 - заменить свой старый клинок, с "
        "уроном",
        str(attack),
        "\n" "2 - пройти мимо",
    )
    print("MEЧ", str(increase_attack), "\n")
    current_choice = choice_player()

    if current_choice == "1":
        attack = increase_attack
        print(
            "вы подобрали клинок и выбросили старый, ваша атака героя стала равна",
            str(increase_attack),
        )
    elif current_choice == "2":
        print("вы отказались подбирать клинок, ваша атака не изменилась")


def skirmish_with_monster() -> None:
    """Встреча с монстром."""
    global monster_counter
    global hp
    print(
        "БОЙ",
        str(monster_hp),
        str(monster_attack),
        "\n1 - атаковать чудовище\n2 - убежать",
    )
    fight_or_not = choice_player()
    if fight_or_not == "1":
        print("деремся")
        fight()
        if hp > 0:
            monster_counter = monster_counter + 1
        else:
            print("ПОРАЖЕНИЕ")
            sys.exit()

    elif fight_or_not == "2":
        dices = random.randint(0, hp)
        hp = hp - dices
        print("ты убежал, при этом потерял", dices, "единиц здоровья\n")


def fight() -> None:
    """Сражение с монстром."""
    global hp
    global monster_hp

    while hp > 0 and monster_hp > 0:
        hp = hp - monster_attack
        monster_hp = monster_hp - attack


def game() -> None:
    """Процесс игры."""
    global monster_counter
    global hp
    while monster_counter <= 10 or hp >= 1:
        print("")
        print("Монстров убито", str(monster_counter))
        hero()
        events = random.randint(1, 3)
        if events == 1:
            apple()
        elif events == 2:
            monster()
            skirmish_with_monster()
            if hp <= 0:
                print("ПОРАЖЕНИЕ")
                sys.exit()
        else:
            sword()

        if hp <= 0 or monster_counter == 10:
            break

    if monster_counter == 10 and hp > 0:
        print("ПОБЕДА")
        sys.exit()
    elif hp <= 0 and monster_counter <= 10:
        print("ПОРАЖЕНИЕ")
        sys.exit()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Использование нативных типов данных С.
# Quake Fast Inverse Square Root algorithm
# Оригинал https://github.com/ajcr/ajcr.github.io/blob/master/_posts/2016-04-01-fast-inverse-square-root-python.md

from ctypes import c_float, c_int32, cast, byref, POINTER

def ctypes_isqrt(number):
    threehalfs = 1.5
    x2 = number * 0.5
    y = c_float(number)

    i = cast(byref(y), POINTER(c_int32)).contents.value
    i = c_int32(0x5f3759df - (i >> 1))
    y = cast(byref(i), POINTER(c_float)).contents.value

    y = y * (1.5 - (x2 * y * y))
    return y

# Используя struct

def struct_isqrt(number):
    threehalfs = 1.5
    x2 = number * 0.5
    y = number
    
    packed_y = struct.pack('f', y)       
    i = struct.unpack('i', packed_y)[0]  # treat float's bytes as int 
    i = 0x5f3759df - (i >> 1)            # arithmetic with magic number
    packed_i = struct.pack('i', i)
    y = struct.unpack('f', packed_i)[0]  # treat int's bytes as float
    
    y = y * (threehalfs - (x2 * y * y))  # Newton's method
    return y

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# поиск "кратеров" из изображения - матрица нулей и единиц. Рекурсивный поиск в глубину
# Код простой, но сама задача интересная
# кратер - набор смежных единиц в матрице

def calculate(my_input: list) -> int:
    """Расчитывает количество лунных кратеров."""
    row = len(my_input)
    col = len(my_input[0])
    count = 0

    for i in range(row):
        for j in range(col):
            if my_input[i][j] == 1:
                passing_deep(my_input, row, col, i, j)
                count += 1
    return count


def passing_deep(my_input: list, row: int, col: int, x: int, y: int) -> None:
    """Проверяет каждый элемент и проходит по матрице."""
    if my_input[x][y] == 0:
        return None
    my_input[x][y] = 0

    if x != 0:
        passing_deep(my_input, row, col, x - 1, y)
    if x != row - 1:
        passing_deep(my_input, row, col, x + 1, y)
    if y != 0:
        passing_deep(my_input, row, col, x, y - 1)
    if y != col - 1:
        passing_deep(my_input, row, col, x, y + 1)
